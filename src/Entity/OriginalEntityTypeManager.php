<?php

namespace Drupal\multiversion_remover\Entity;

use Drupal\Core\Entity\EntityTypeManager;

/**
 * An entity type repository for retrieving how entities are defined in code.
 *
 * Unlike the EntityTypeManager in core, this implementation does not rely on
 * any caching or alter hooks and instead retrieves entity type definitions as
 * they are exposed by discovering and parsing entity type annotations. This is
 * used to obtain information that can be used for comparison against the state
 * of entities as they presently exist in the system to determine if the
 * entities match their pristine "stock" configuration.
 */
class OriginalEntityTypeManager extends EntityTypeManager {

  /**
   * {@inheritdoc}
   *
   * This implementation does not make use of caching, so that we are always
   * reading information as it appears in annotations rather than what may have
   * been read and altered by the core Entity Type Manager service.
   *
   * @throws \InvalidArgumentException
   *   If $use_caches is set to anything other than FALSE.
   */
  public function useCaches($use_caches = FALSE) {
    if ($use_caches === TRUE) {
      throw new \InvalidArgumentException(
        'Caches cannot be used with this plug-in manager.'
      );
    }

    parent::useCaches($use_caches);
  }

  /**
   * {@inheritdoc}
   *
   * The definition is read using discovery, bypassing alter hooks and all forms
   * of caching, to ensure that it matches what's defined in code.
   */
  public function getDefinition($entity_type_id, $exception_on_invalid = TRUE) {
    $this->useCaches(FALSE);

    return parent::getDefinition($entity_type_id, $exception_on_invalid);
  }

  /**
   * {@inheritdoc}
   *
   * The definitions are read using discovery, bypassing alter hooks and all
   * forms of caching, to ensure that they match what's defined in code.
   */
  public function getDefinitions() {
    $this->useCaches(FALSE);

    return parent::getDefinitions();
  }

  /**
   * {@inheritdoc}
   *
   * This implementation avoids invoking alter hooks, so that we only return
   * info as it has been defined on type annotations.
   */
  protected function findDefinitions() {
    return $this->getDiscovery()->getDefinitions();
  }

}
